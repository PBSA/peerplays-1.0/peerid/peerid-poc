process.env.NODE_ENV = 'test';
const request = require('supertest');
const chai = require('chai');
chai.use(require('chai-url'));
const chaiHttp = require('../helpers/node_modules/chai-http');
const {isSuccess, isError} = require('../helpers/test.response.helper');
const {login} = require('../helpers/test.login.helper');
const ApiModule = require('../../../api.module.test');

chai.use(chaiHttp);
let agent;
let apiModule;

before(async () => {
  apiModule = await ApiModule();
  agent = request.agent(apiModule.app);
});

describe('POST /api/v1/auth/logout', () => {

  const validObject = {
    email: 'testlogout@email.com',
    username: 'test-testlogout',
    password: 'MyPassword^007',
    repeatPassword: 'MyPassword^007'
  };

  beforeEach(async () => {
    await agent.post('/api/v1/auth/logout');
  });

  it('should forbid. only authorized user', async () => {
    const response = await agent.post('/api/v1/auth/logout');
    isError(response, 401);
  });

  it('should success. valid request', async () => {
    await login(agent, validObject, apiModule);
    const response = await agent.post('/api/v1/auth/logout');
    isSuccess(response);
  });

});

after(async () => {
  await apiModule.close();
});
