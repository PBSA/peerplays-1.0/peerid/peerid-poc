const awilix = require('awilix');
/** @type AppConfig */
const CONFIG = require('config');
const path = require('path');
const {getLogger} = require('log4js');

const {
  Lifetime, InjectionMode, asClass, asValue
} = awilix;

const logger = getLogger();
logger.level = CONFIG.logLevel;

// create awilix container
const container = awilix.createContainer({
  injectionMode: InjectionMode.PROXY
});

// load modules
container.loadModules([
  ['src/services/*.js', {register: asClass}],
  ['src/helpers/*.js', {register: asClass}],
  ['src/connections/*.js', {register: asClass}],
  ['src/repositories/*.js', {register: asClass}],
  ['src/controllers/*.js', {register: asClass}],
  ['src/validators/*.js', {register: asClass}]
], {
  formatName: 'camelCase',
  resolverOptions: {
    lifetime: Lifetime.SINGLETON,
    injectionMode: InjectionMode.PROXY
  }
});

// init workers with proxy mode for simple prototyping
container.loadModules([
  ['workers/*.js', {register: asClass}]
], {
  formatName: 'camelCase',
  resolverOptions: {
    lifetime: Lifetime.SINGLETON,
    injectionMode: InjectionMode.PROXY
  }
});

CONFIG.isDevelopment = process.env.NODE_ENV === 'development';

CONFIG.basePath = path.resolve(__dirname, '../');

container.register({
  config: asValue(CONFIG),
  basePath: asValue(__dirname)
});

async function initModule(name, mode) {
  const scope = container.createScope();
  scope.loadModules([
    ['src/*.module.js', {register: asClass}]
  ], {
    formatName: 'camelCase',
    resolverOptions: {
      lifetime: Lifetime.SCOPED,
      resolutionMode: InjectionMode.PROXY
    }
  });
  const module = scope.resolve(name.replace(/\.([a-z])/, (a) => a[1].toUpperCase()));

  if (typeof module.initModule === 'function') {
    logger.info(`Init ${name.replace(/\.[a-z]+$/, '')} module`);
    await module.initModule(mode);
  }

  return module;
}

module.exports = {
  initModule,
  container
};
