class AuthController {

  /**
   * @param {AuthValidator} opts.authValidator
   * @param {UserService} opts.userService
   * @param {AppService} opts.appService
   */
  constructor(opts) {
    this.authValidator = opts.authValidator;
    this.userService = opts.userService;
    this.appService = opts.appService;
  }

  /**
   * Array of routes processed by this controller
   * @returns {*[]}
   */
  getRoutes() {
    return [
      [
        'post',
        '/api/v1/auth/logout',
        this.authValidator.loggedOnly,
        this.logout.bind(this)
      ],
      [
        'post',
        '/api/v1/auth/peerplays',
        this.authValidator.validatePeerplaysLogin,
        this.peerplaysLogin.bind(this)
      ],
      [
        'post',
        '/api/v1/auth/exchange',
        this.authValidator.validateCode,
        this.exchangeCode.bind(this)
      ]
    ];
  }

  async logout(user, data, req) {
    req.logout();
    return true;
  }

  async peerplaysLogin(_, {login, password, appId}, req) {
    const data = await this.userService.loginPeerplaysUser(login, password, req.user, appId);
    await new Promise((success) => req.login(data.user, () => success()));
    return data;
  }

  async exchangeCode(user, {grantCodeId, appId, scope}) {
    return await this.appService.createAccessToken(grantCodeId, appId, user.id, scope);
  }
}

module.exports = AuthController;
