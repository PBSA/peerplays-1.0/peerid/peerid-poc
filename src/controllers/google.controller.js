const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

class GoogleController {

  /**
   * @param {UserService} opts.userService
   * @param {AppConfig} opts.config
   */
  constructor(opts) {
    this.userService = opts.userService;
    this.config = opts.config;

    this.DEFAULT_SCOPE = [
      'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email'
    ];
  }

  /**
   * Array of routes processed by this controller
   * @returns {*[]}
   */
  getRoutes(app) {
    this.initializePassport();
    app.get('/api/v1/auth/google', (req) => {
      if (req.query.client_id && req.query.redirect_uri) {
        req.session.appId = req.query.client_id;
        req.session.redirectURI = req.query.redirect_uri;
        req.session.save();
      }

      passport.authenticate('google', {
        scope: this.DEFAULT_SCOPE,
        access_type: 'offline'
      });
    });

    app.get('/api/v1/auth/google/callback', (req, res) => {
      passport.authenticate(
        'google',
        {failureRedirect: `${this.config.frontendUrl}/error?google-auth-error=restrict`}
      )(req, res, (err) => {

        if (err) {
          res.redirect(`${this.config.frontendUrl}/error?google-auth-error=${err.message}`);
          return;
        }

        if(req.session.redirectURI) {
          const code = req.session.code || '';
          res.redirect(`${req.session.redirectURI}?code=${code}`);
        }else {
          res.redirect(`${this.config.frontendCallbackUrl}`);
        }
      });

    });

    return [];
  }

  initializePassport() {
    passport.use(new GoogleStrategy({
      passReqToCallback: true,
      clientID: this.config.google.clientId,
      clientSecret: this.config.google.clientSecret,
      callbackURL: `${this.config.backendUrl}/api/v1/auth/google/callback`
    }, (req, accessToken, profile, done) => {
      this.userService.getUserBySocialNetworkAccount('google', {
        id: profile.id,
        ...profile._json,
        username: profile._json.email.replace(/@.+/, '')
      }, accessToken, req).then((User) => {
        this.userService.getCleanUser(User).then((user) => done(null, user));
      }).catch((error) => {
        done(error);
      });
    }));
  }

}

module.exports = GoogleController;
