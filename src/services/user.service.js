const crypto = require('crypto');
const {Login} = require('peerplaysjs-lib');
const RestError = require('../errors/rest.error');
const PeerplaysNameExistsError = require('./../errors/peerplays-name-exists.error');
const logger = require('log4js').getLogger('user.service');
const url = require('url');
const uid = require('uid2');

const IS_PRODUCTION = process.env.NODE_ENV === 'production';

class UserService {

  /**
     * @param {UserRepository} opts.userRepository
     * @param {PeerplaysRepository} opts.peerplaysRepository
     * @param {AppRepository} opts.appRepository
     * @param {OperationRepository} opts.operationRepository
     * @param {PermissionRepository} opts.permissionRepository
     * @param {GrantCodeRepository} opts.grantCodeRepository
     * @param {MailService} opts.mailService
     */
  constructor(opts) {
    this.config = opts.config;
    this.userRepository = opts.userRepository;
    this.peerplaysRepository = opts.peerplaysRepository;
    this.appRepository = opts.appRepository;
    this.operationRepository = opts.operationRepository;
    this.permissionRepository = opts.permissionRepository;
    this.grantCodeRepository = opts.grantCodeRepository;
    this.mailService = opts.mailService;
  }

  /**
   *
   * @param {String} username
   * @param {Number} numRetries
   */
  async createUserForSocialNetwork(username, numRetries = 0) {
    const MAX_RETRIES = 5;

    if (numRetries >= MAX_RETRIES) {
      throw new Error('failed to create user, too many retries');
    }

    const randomString = `${Math.floor(Math.min(1000 + Math.random() * 9000, 9999))}`; // random 4 digit number

    try {
      const user = await this.userRepository.model.create({
        username: numRetries === 0 ? username : `${username}-${randomString}`
      });
      return user;
    } catch (err) {
      return this.createUserForSocialNetwork(username, numRetries + 1);
    }
  }

  /**
     * @param {String} username
     * @param {String} password
     * @param {Number} numRetries
     */
  async createPeerplaysAccountForSocialNetwork(username, password, numRetries = 0) {
    const MAX_RETRIES = 5;

    if (numRetries >= MAX_RETRIES) {
      throw new Error('failed to create peerplays account, too many retries');
    }

    const hash = crypto.createHash('sha256').digest(username).toString('hex').slice(0, 32);
    const randomString = `${Math.floor(Math.min(1000 + Math.random() * 9000, 9999))}`; // random 4 digit number
    const seUsername = numRetries === 0 ? `id-${hash}` : `id-${hash}-${randomString}`;

    const keys = Login.generateKeys(
      seUsername,
      password,
      ['owner', 'active'],
      IS_PRODUCTION ? 'PPY' : 'TEST'
    );

    const ownerKey = keys.pubKeys.owner;
    const activeKey = keys.pubKeys.active;

    try {
      logger.info('creating peerplays account with username: ' + seUsername);
      return await this.peerplaysRepository.createPeerplaysAccount(seUsername, ownerKey, activeKey);
    } catch (err) {
      if (err instanceof PeerplaysNameExistsError) {
        return this.createPeerplaysAccountForSocialNetwork(username, password, numRetries + 1);
      }

      throw err;
    }
  }

  /**
     * Find user by network account id and create row if not exists
     * @param {String} network
     * @param account
     * @param {String} accessToken
     * @param {} req
     * @returns {Promise<UserModel>}
     */
  async getUserBySocialNetworkAccount(network, account, accessToken, req) {
    const loggedUser = req.user;
    req.session.newUser = false;
    req.session.save();

    const {id, email, username} = account;
    const {appId, redirectURI} = req.session;

    const UserWithNetworkAccount = await this.userRepository.model.findOne({where: {[`${network}Id`]: id}});

    if (UserWithNetworkAccount && loggedUser && loggedUser.id !== UserWithNetworkAccount.id) {
      throw new Error('this account is already connected to another profile');
    }

    if (loggedUser) {
      return this.connectSocialNetwork(network, account, loggedUser);
    }

    if (UserWithNetworkAccount) {
      return UserWithNetworkAccount;
    }

    req.session.newUser = true;
    req.session.save();

    const emailIsUsed = email && await this.userRepository.model.count({where: {email}});
    const User = await this.createUserForSocialNetwork(username);

    User[`${network}Id`] = id;
    User.email = emailIsUsed ? null : email;
    User.googleName = network === 'google' ? username : '';
    User.facebook = network === 'facebook' ? username : '';

    await User.save();
    const peerplaysPassword = `${User.username}-${accessToken}`;
    const peerplaysAccount = await this.createPeerplaysAccountForSocialNetwork(User.username, peerplaysPassword);

    User.peerplays_account_name = peerplaysAccount.name;
    User.peerplays_account_id = await this.peerplaysRepository.getAccountId(peerplaysAccount.name);

    this.mailService.sendMailAfterRegistration(peerplaysPassword);

    if(appId && redirectURI) {
      const App = await this.appRepository.findByPk(appId);

      if(App) {
        await this.verifyApp(App, redirectURI);
        await this.joinAppUser(App, User, peerplaysPassword);
        const code = await this.getGrantCode(App, User);

        req.session.code = code;
        req.session.save();
      } else {
        throw new Error('App doesn\'t exist');
      }
    }

    return User.save();
  }

  async getGrantCode(app, User) {
    const Ops = await this.operationRepository.findAll({
      where: {app_id: app.id}
    });

    const OpsArr = Ops.map(({operation_requested}) => operation_requested);

    const code = await this.generateUniqueGrantCode();

    await this.grantCodeRepository.create({
      app_id: app.id,
      user_id: User.id,
      scope: OpsArr,
      code
    });

    return code;
  }

  async generateUniqueGrantCode() {
    const code = uid(24);

    const grant = await this.grantCodeRepository.findOne({
      where: {code}
    });

    if(!grant) {
      return await this.generateUniqueGrantCode();
    }

    return code;
  }

  async verifyApp(App, redirectURI) {
    let match = false, uri = url.URL(redirectURI || '');

    for (let i = 0; i < App.domains.length; i++) {
      if (uri.host == App.domains[i] || (uri.protocol == App.domains[i] && uri.protocol != 'http' && uri.protocol != 'https')) {
        match = true;
        break;
      }
    }

    if(!match) {
      throw new Error('You must supply a redirect_uri that is a domain or url scheme owned by your app');
    }
  }

  async connectSocialNetwork(network, account, User) {
    const {id, email, username} = account;

    if (User[`${network}Id`] === id) {
      return User;
    }

    const emailIsUsed = email && await this.userRepository.model.count({where: {email}});
    const usernameIsUsed = username && await this.userRepository.model.count({where: {username}});
    User[`${network}Id`] = id;

    if (network === 'google') {
      User.googleName = username;
    } else if (network === 'facebook') {
      User.facebook = username;
    } else {
      throw new RestError(`Unexpected Network ${network}`);
    }

    if (!User.email && !emailIsUsed) {
      User.email = email;
    }

    if (!User.username && !usernameIsUsed) {
      User.username = username;
    }

    return User.save();
  }

  async joinAppUser(app, user, peerplaysPassword) {
    const Ops = await this.operationRepository.findAll({
      where: {app_id: app.id}
    });

    try {
      const customPermission = await this.peerplaysRepository.createAndSendTransaction('custom_permission_create',{
        fee: {
          amount: 0,
          asset_id: this.config.peerplays.feeAssetId
        },
        owner_account: user.peerplays_account_id,
        permission_name: app.appname.toLowerCase().replace(/ /g,'').substring(0,10),
        auth: {
          weight_threshold: 1,
          account_auths: [[this.config.peerplays.paymentAccountID, 1]],
          key_auths: [],
          address_auths: []
        }
      }, user.peerplays_account_name, peerplaysPassword);

      let today = new Date();
      let year = today.getFullYear();
      let month = today.getMonth();
      let day = today.getDate();
      let anYearFromNow = new Date(year + 1, month, day);

      for(let i = 0; i < Ops.length; i++) {
        const customAuth = await this.peerplaysRepository.createAndSendTransaction('custom_account_authority_create', {
          fee: {
            amount: 0,
            asset_id: this.config.peerplays.feeAssetId
          },
          permission_id: customPermission.trx.operation_results[0][1],
          operation_type: Ops[i].operation_requested,
          valid_from: Math.floor(new Number(today)/1000),
          valid_to: Math.floor(new Number(anYearFromNow)/1000),
          owner_account: user.peerplays_account_id
        },user.peerplays_account_name, peerplaysPassword);

        await this.permissionRepository.model.create({
          peerplays_permission_id: customPermission.trx.operation_results[0][1],
          peerplays_account_auth_id: customAuth.trx.operation_results[0][1],
          operation: Ops[i].operation_requested,
          expiry: anYearFromNow,
          app_id: app.id,
          user_id: user.id
        });
      }
    } catch(err) {
      logger.error(err);
      throw new Error('Peerplays HRP Error');
    }
  }

  /**
     * @param {UserModel} User
     * @returns {Promise<UserPublicObject>}
     */
  async getCleanUser(User) {
    return User.getPublic();
  }

  /**
     * @param {UserModel} User
     * @param name
     * @param activeKey
     * @param ownerKey
     * @returns {Promise<UserModel>}
     */
  async createPeerplaysAccount(User, {name, activeKey, ownerKey}) {
    try {
      await this.peerplaysRepository.createPeerplaysAccount(name, ownerKey, activeKey);
    } catch (details) {
      let error = details;

      if(error.base){
        error = {
          message: error.base.length > 0 ? error.base : 'Invalid active or owner key. '
        };
      }

      throw new RestError('Request error', 400, error);
    }

    User.peerplays_account_name = name;
    await User.save();
    return this.getCleanUser(User);
  }


  /**
   * @param {UserModel} User
   * @returns {Promise<UserPublicObject>}
   */
  async getCleanUserForSearch(User) {
    return User.getPublicMinimal();
  }

  /**
     * Get a list of users corresponding to the specified parameters
     *
     * @param search
     * @param limit
     * @param skip
     * @returns {Promise<[UserModel]>}
     */
  async searchUsers(search, limit, skip) {
    const users = await this.userRepository.searchUsers(search, limit, skip);
    return Promise.all(users.map(async (User) => this.getCleanUserForSearch(User)));
  }

  async loginPeerplaysUser(login, password, LoggedUser = null, appId = null) {
    const PeerplaysUser = await this.peerplaysRepository.getPeerplaysUser(login, password);

    if (!PeerplaysUser) {
      throw new RestError('', 400, {login: [{message: 'Invalid peerplays account'}]});
    }

    const userWithPeerplaysAccount = await this.userRepository.getBypeerplays_account_name(login);

    if (userWithPeerplaysAccount && LoggedUser && LoggedUser.id !== userWithPeerplaysAccount.id) {
      throw new RestError('', 409, {image: [{login: 'This account is already connected to another profile'}]});
    }

    if(userWithPeerplaysAccount) {
      const user = await this.getCleanUser(userWithPeerplaysAccount);
      user['newUser'] = false;
      return user;
    }

    //If the user is already logged in and no peerplays account is linked then link this account
    if(LoggedUser && !userWithPeerplaysAccount) {
      LoggedUser.peerplays_account_name = login;
      LoggedUser.peerplays_account_id = PeerplaysUser[1].account.id;
      LoggedUser.peerplaysMasterPassword = '';
      await LoggedUser.save();
      const user = await this.getCleanUser(LoggedUser);
      user['newUser'] = false;
      return user;
    }

    const NewUser = await this.userRepository.model.create({
      username: await this.getUsernameForPeerplaysAccount(login),
      password,
      peerplays_account_name: login,
      peerplays_account_id: PeerplaysUser[1].account.id
    });

    let code;

    if(appId) {
      if(appId) {
        const App = await this.appRepository.findByPk(appId);
  
        if(App) {
          await this.joinAppUser(App, NewUser, password);
          code = await this.getGrantCode(App, NewUser);
  
        } else {
          throw new Error('App doesn\'t exist');
        }
      }
    }

    await NewUser.save();

    const user = await this.getCleanUser(NewUser);
    user['newUser'] = true;
    return {user, code};
  }

  async getUsernameForPeerplaysAccount(accountName, numRetries=0){
    const MAX_RETRIES = 5;
    let username = accountName;

    if (numRetries >= MAX_RETRIES) {
      throw new RestError('Failed to create user, too many retries',400);
    }

    const UsernameExists = await this.userRepository.getByLogin(username);

    if(UsernameExists) {
      const randomString = `${Math.floor(Math.min(1000 + Math.random() * 9000, 9999))}`; // random 4 digit number
      username = this.getUsernameForPeerplaysAccount(accountName + randomString, numRetries + 1);
    }

    return username;
  }

}

module.exports = UserService;
