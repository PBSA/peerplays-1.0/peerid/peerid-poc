const uid = require('uid2');

class AppService {

  /**
     * @param {Config} opts.config
     * @param {AppRepository} opts.appRepository
     * @param {OperationRepository} opts.operationRepository
     * @param {PermissionRepository} opts.permissionRepository
     * @param {AccessTokenRepository} opts.accessTokenRepository
     * @param {PeerplaysRepository} opts.peerplaysRepository
     */
  constructor(opts) {
    this.config = opts.config;
    this.appRepository = opts.appRepository;
    this.operationRepository = opts.operationRepository;
    this.permissionRepository = opts.permissionRepository;
    this.accessTokenRepository = opts.accessTokenRepository;
    this.peerplaysRepository = opts.peerplaysRepository;
  }

  async registerApp(data, user_id) {
    const appSecret = await this.generateUniqueAppSecret();
    const App = await this.appRepository.model.create({
      ...data,
      registrar_id: user_id,
      app_secret: appSecret
    });

    await Promise.all(data.operations.map(async (operation) => {
      await this.operationRepository.model.create({
        app_id: App.id,
        operation_requested: operation
      });
    }));

    return App.getPublic();
  }

  async generateUniqueAppSecret() {
    const uuid = uid(42);

    const UIDExists = await this.appRepository.model.findOne({
      where: {
        app_secret: uuid
      }
    });

    if(UIDExists) {
      return await this.generateUniqueAppSecret();
    }

    return uuid;
  }

  async getApps(user_id) {
    return this.appRepository.findAll({
      where: { 
        registrar_id: user_id
      }
    });
  }

  async deleteApp(id) {
    const deleted = await this.appRepository.delete({
      where: {id}
    });

    if(deleted !== 0) {
      await this.operationRepository.delete({
        where: {
          app_id: id
        }
      });

      await this.permissionRepository.delete({
        where: {
          app_id: id
        }
      });
    }

    return deleted !== 0;
  }

  async createAccessToken(grantCodeId, appId, userId, scope) {
    const token = await this.generateUniqueAccessToken();
    return this.accessTokenRepository.create({
      app_id: appId,
      grantcode_id: grantCodeId,
      user_id: userId,
      scope,
      token
    });
  }

  async generateUniqueAccessToken() {
    const uuid = uid(124);

    const AccessTokenExists = await this.accessTokenRepository.model.findOne({
      where: {
        token: uuid
      }
    });

    if(AccessTokenExists) {
      return await this.generateUniqueAccessToken();
    }

    return uuid;
  }

  async broadcastTransaction(transaction) {
    return this.peerplaysRepository.broadcastSerializedTx(transaction);
  }
}

module.exports = AppService;