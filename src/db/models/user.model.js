const Sequelize = require('sequelize');
const {Model} = Sequelize;

/**
 * @typedef {Object} UserPublicObject
 * @property {Number} id
 * @property {String} username
 * @property {String} email
 * @property {String} googleName
 * @property {String} facebook
 * @property {String} peerplays_account_name
 */

/**
 * @typedef {Class} UserModel
 * @property {Number} id
 * @property {String} username
 * @property {String} email
 * @property {String} googleId
 * @property {String} facebookId
 * @property {String} googleName
 * @property {String} facebook
 * @property {String} peerplays_account_name
 * @property {String} peerplays_account_id
 */
class UserModel extends Model {
  /**
   * @returns {UserPublicObject}
   */
  getPublic() {
    return {
      id: this.id,
      username: this.username || '',
      email: this.email || '',
      googleName: this.googleName,
      facebook: this.facebook,
      peerplays_account_name: this.peerplays_account_name
    };
  }

  getPublicMinimal() {
    return {
      id: this.id,
      username: this.username || '',
      peerplays_account_name: this.peerplays_account_name
    };
  }
}
const attributes = {
  username: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: true
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: true
  },
  googleId: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: true
  },
  facebookId: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: true
  },
  googleName: {
    type: Sequelize.STRING
  },
  facebook: {
    type: Sequelize.STRING,
    defaultValue: ''
  },
  peerplays_account_name: {
    type: Sequelize.STRING,
    defaultValue: ''
  },
  peerplays_account_id: {
    type: Sequelize.STRING,
    defaultValue: ''
  }
};

module.exports = {
  init: (sequelize) => {
    UserModel.init(attributes, {
      sequelize,
      modelName: 'users'
    });
  },
  associate: () => {
  },
  get model() {
    return UserModel;
  }
};
