const Sequelize = require('sequelize');

const {model} = require('../db/models/user.model');
const BasePostgresRepository = require('./abstracts/base-postgres.repository');

class UserRepository extends BasePostgresRepository {

  constructor() {
    super(model);
  }

  /**
   * @param ids
   * @returns {Promise<UserModel[]>}
   */
  async findByPkList(ids) {
    return this.model.findAll({
      where: {
        id: {[Sequelize.Op.in]: ids}
      }
    });
  }

  async getByLogin(login) {
    return this.model.findOne({
      where: {
        [Sequelize.Op.or]: [{
          email: login.toLowerCase()
        }, {
          username: login
        }]
      }
    });
  }

  async searchUsers(search, limit, offset) {
    const filter = search ? {
      username: {
        [Sequelize.Op.like]: `%${search}%`
      }
    } : null;
    return this.model.findAll({
      where: filter,
      offset,
      limit
    });
  }

  async getBypeerplays_account_name(accountName) {
    return this.model.findOne({
      where: {
        peerplays_account_name: accountName
      }
    });
  }

}

module.exports = UserRepository;
